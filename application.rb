require 'grape'
module Worker
	class API < Grape::API
		version 'v1' 
		format :json
		prefix :api

		resource :workers do
      desc 'list all jobs'
			get do
        'all jobs'
			end
      route_param :uuid do
        desc 'create worker'
        params do
          requires :cfg, type: Rack::Multipart::UploadedFile, desc: 'a cfg file that the worker runs'
        end

        post do
          status :accepted

          io = params[:cfg]
          uuid = params[:uuid]
          dir = 'jobs/' + uuid.to_s
          new_file_name = dir + '/cfg.json'
          FileUtils.mkdir_p dir
          FileUtils.cp io.tempfile.path, new_file_name
          dir
        end
      end
		end
	end
end
